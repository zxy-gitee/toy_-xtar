#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/ide.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/of_gpio.h>
#include <linux/semaphore.h>
#include <linux/timer.h>
#include <linux/i2c.h>
#include <asm/mach/map.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include "mpu6050reg.h"
/***************************************************************
Copyright © ALIENTEK Co., Ltd. 1998-2029. All rights reserved.
文件名		: mpu6050.c
作者	  	: zxy
版本	   	: V1.0
描述	   	: mpu6050驱动程序
日志	   	: 初版V1.0 2022/2/16 zxy修改
***************************************************************/
#define MPU6050_CNT	1
#define MPU6050_NAME	"mpu6050"

struct mpu6050_dev {
	dev_t devid;				/* 设备号 	 */
	struct cdev cdev;			/* cdev 	*/
	struct class *class;		/* 类 		*/
	struct device *device;		/* 设备 	 */
	struct device_node	*nd; 	/* 设备节点 */
	int major;					/* 主设备号 */
	void *private_data;			/* 私有数据 */
	//接收的数据为2字节 但i2c的基本单位都是1字节
	signed short gyro_x_adc;		/* 陀螺仪X轴原始值 	 */
	signed short gyro_y_adc;		/* 陀螺仪Y轴原始值		*/
	signed short gyro_z_adc;		/* 陀螺仪Z轴原始值 		*/
	signed short accel_x_adc;		/* 加速度计X轴原始值 	*/
	signed short accel_y_adc;		/* 加速度计Y轴原始值	*/
	signed short accel_z_adc;		/* 加速度计Z轴原始值 	*/
	signed short temp_adc;		/* 温度原始值 			*/
};

static struct mpu6050_dev mpu6050dev;

//-----------------------------------------------------------------------------------------------------------------------//
//mpu6050初始化等操作函数

/*
 * @description	: 从mpu6050读取多个寄存器数据
 * @param - dev:  mpu6050设备
 * @param - reg:  要读取的寄存器首地址 8bit
 * @param - val:  读取到的数据
 * @param - len:  要读取的数据长度
 * @return 		: 操作结果
 */
static int mpu6050_read_regs(struct mpu6050_dev *dev, u8 reg, void *val, int len)
{
	int ret;
	struct i2c_msg msg[2];												//一个msg发送时序中的>=2Byte 用于存从机地址+数据
	struct i2c_client *client = (struct i2c_client *)dev->private_data;	//结构体中存放从机地址，用到的i2c控制器信息

	/* msg[0] 2 Byte：从机地址+写+寄存机地址 */
	msg[0].addr = client->addr;			/* mpu6050地址 7bit */
	msg[0].flags = 0;					/* 0为写 1bit */
	msg[0].buf = &reg;					/* 读取的首地址 8bit */
	msg[0].len = 1;						/* reg长度*/

	/* msg[1] 1+n_data Byte：从机地址+读+本地接收地址 */
	msg[1].addr = client->addr;			/* mpu6050地址 */
	msg[1].flags = I2C_M_RD;			/* 1为读 */
	msg[1].buf = val;					/* 读取数据缓冲区 */
	msg[1].len = len;					/* 要读取的数据长度*/

	ret = i2c_transfer(client->adapter, msg, 2);//拜托i2c控制器发送相关信息 i2c1或i2c2
	if(ret == 2) {
		ret = 0;
	} else {
		printk("i2c rd failed=%d reg=%06x len=%d\n",ret, reg, len);
		ret = -EREMOTEIO;
	}
	return ret;
}

/*
 * @description	: 读取mpu6050指定寄存器值，读取一个寄存器
 * @param - dev:  mpu6050设备
 * @param - reg:  要读取的寄存器
 * @return 	  :   读取到的寄存器值
 */
static unsigned char mpu6050_read_reg(struct mpu6050_dev *dev, u8 reg)
{
	u8 data = 0;

	mpu6050_read_regs(dev, reg, &data, 1);
	return data;

#if 0
	struct i2c_client *client = (struct i2c_client *)dev->private_data;
	return i2c_smbus_read_byte_data(client, reg);
#endif
}

/*
 * @description	: 向mpu6050多个寄存器写入数据
 * @param - dev:  mpu6050设备
 * @param - reg:  要写入的寄存器首地址
 * @param - buf:  要写入的数据缓冲区
 * @param - len:  要写入的数据长度
 * @return 	  :   操作结果
 */
static s32 mpu6050_write_regs(struct mpu6050_dev *dev, u8 reg, u8 *buf, u8 len)
{
	u8 b[256];
	struct i2c_msg msg;
	struct i2c_client *client = (struct i2c_client *)dev->private_data;
	
	b[0] = reg;					/* 寄存器首地址 */
	memcpy(&b[1],buf,len);		/* 将要写入的数据拷贝到数组b里面 */

	/* msg：1+n_data Byte：从机地址+读+本地接收地址 */
	msg.addr = client->addr;	/* mpu6050地址 7bit */
	msg.flags = 0;				/* 0为写 1bit */
	msg.buf = b;				/* 要写入的数据缓冲区 */
	msg.len = len + 1;			/* 要写入的数据长度 */

	return i2c_transfer(client->adapter, &msg, 1);
}

/*
 * @description	: 向mpu6050指定寄存器写入指定的值，写一个寄存器
 * @param - dev:  mpu6050设备
 * @param - reg:  要写的寄存器
 * @param - data: 要写入的值
 * @return   :    无
 */
static void mpu6050_write_reg(struct mpu6050_dev *dev, u8 reg, u8 data)
{
	u8 buf = 0;//新建一个变量，可能更安全吧
	buf = data;
	mpu6050_write_regs(dev, reg, &buf, 1);
}

/*
 * @description	: mpu6050相关寄存器的初始化
 * @return 		: 无。
 */
void mpu6050_reginit(void)
{
	u8 value = 0;
	
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_PWR_MGMT_1, 0x80);
	mdelay(50);
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_PWR_MGMT_1, 0x01);
	mdelay(50);

	value = mpu6050_read_reg(&mpu6050dev, MPU6050_RA_WHO_AM_I);
	printk("mpu6050 ID = %#X\r\n", value);	

	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_SMPLRT_DIV, 0x00); 	/* 输出速率是内部采样率					*/
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_GYRO_CONFIG, 0x18); 	/* 陀螺仪±2000dps量程 				*/
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_ACCEL_CONFIG, 0x18); 	/* 加速度计±16G量程 					*/
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_CONFIG, 0x04); 		/* 陀螺仪低通滤波BW=20Hz 				*/
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_FF_THR, 0x04); 		/* 加速度计低通滤波BW=21.2Hz 			*/
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_PWR_MGMT_2, 0x00); 	/* 打开加速度计和陀螺仪所有轴 				*/
	// mpu6050_write_reg(&mpu6050dev, MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_CYCLE_BIT); 	/* 关闭低功耗 						*/
	mpu6050_write_reg(&mpu6050dev, MPU6050_RA_FIFO_EN, 0x00);		/* 关闭FIFO						*/
}

/*
 * @description	: 读取mpu6050的数据，读取原始数据
 * @return 		: 无。
 */
void mpu6050_readdata(struct mpu6050_dev *dev)
{
	unsigned char data[14];	
	mpu6050_read_regs(dev, MPU6050_RA_ACCEL_XOUT_H, data, 14);	//0x3B位置，连续14个寄存器，包括加速度、温度、角速度等数据
	//设备结构体收到了数据
	dev->accel_x_adc = (signed short)((data[0] << 8) | data[1]); 
	dev->accel_y_adc = (signed short)((data[2] << 8) | data[3]); 
	dev->accel_z_adc = (signed short)((data[4] << 8) | data[5]); 
	dev->temp_adc    = (signed short)((data[6] << 8) | data[7]); 
	dev->gyro_x_adc  = (signed short)((data[8] << 8) | data[9]); 
	dev->gyro_y_adc  = (signed short)((data[10] << 8) | data[11]);
	dev->gyro_z_adc  = (signed short)((data[12] << 8) | data[13]);
}

//-----------------------------------------------------------------------------------------------------------------------//
//文件操作结构体部分

/*
 * @description		: 打开设备
 * @param - inode 	: 传递给驱动的inode
 * @param - filp 	: 设备文件，file结构体有个叫做private_data的成员变量
 * 					  一般在open的时候将private_data指向设备结构体。
 * @return 			: 0 成功;其他 失败
 */
static int mpu6050_open(struct inode *inode, struct file *filp)
{
	filp->private_data = &mpu6050dev;//在所有文件操作函数中使用设备文件保存该设备结构体

	/* 每次打开设备文件，mpu6050时就初始化一次 */
	mpu6050_reginit();

	return 0;
}

/*
 * @description		: 从设备读取数据 
 * @param - filp 	: 要打开的设备文件(文件描述符)
 * @param - buf 	: 返回给用户空间的数据缓冲区
 * @param - cnt 	: 要读取的数据长度
 * @param - offt 	: 相对于文件首地址的偏移
 * @return 			: 读取的字节数，如果为负值，表示读取失败
 */
static ssize_t mpu6050_read(struct file *filp, char __user *buf, size_t cnt, loff_t *off)
{
	signed short data[7];
	long err = 0;

	struct mpu6050_dev *dev = (struct mpu6050_dev *)filp->private_data;//从文件操作函数领域使用自己保存的设备结构体
	
	mpu6050_readdata(dev);//读到的数据存入了本设备结构体变量中
	//将数据从内核空间发送到用户空间
	data[0] = dev->gyro_x_adc;
	data[1] = dev->gyro_y_adc;
	data[2] = dev->gyro_z_adc;
	data[3] = dev->accel_x_adc;
	data[4] = dev->accel_y_adc;
	data[5] = dev->accel_z_adc;
	data[6] = dev->temp_adc;
	err = copy_to_user(buf, data, sizeof(data));
	return 0;
}

/*
 * @description		: 关闭/释放设备
 * @param - filp 	: 要关闭的设备文件(文件描述符)
 * @return 			: 0 成功;其他 失败
 */
static int mpu6050_release(struct inode *inode, struct file *filp)
{
	return 0;
}

/* mpu6050文件操作函数 */
static const struct file_operations mpu6050_ops = {
	.owner = THIS_MODULE,
	.open = mpu6050_open,
	.read = mpu6050_read,
	.release = mpu6050_release,
};

//-----------------------------------------------------------------------------------------------------------------------//
//设备驱动结构体部分

 /*
  * @description     : i2c驱动的probe函数，当驱动与设备匹配以后此函数就会执行，就是初始化开头的mpu6050结构体中的各个变量      
  * @param - client  : i2c设备
  * @param - id      : i2c设备ID
  * @return          : 0，成功;其他负值,失败
  */
static int mpu6050_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	/* 1、构建设备号 */
	if (mpu6050dev.major) {
		mpu6050dev.devid = MKDEV(mpu6050dev.major, 0);
		register_chrdev_region(mpu6050dev.devid, MPU6050_CNT, MPU6050_NAME);
	} else {
		alloc_chrdev_region(&mpu6050dev.devid, 0, MPU6050_CNT, MPU6050_NAME);
		mpu6050dev.major = MAJOR(mpu6050dev.devid);
	}

	/* 2、注册字符设备 */
	cdev_init(&mpu6050dev.cdev, &mpu6050_ops);		//有了设备号就能注册字符设备，提供文件操作函数并得到了相关字符设备资格
	cdev_add(&mpu6050dev.cdev, mpu6050dev.devid, MPU6050_CNT);

	/* 3、创建类 */
	mpu6050dev.class = class_create(THIS_MODULE, MPU6050_NAME);
	if (IS_ERR(mpu6050dev.class)) {
		return PTR_ERR(mpu6050dev.class);
	}

	/* 4、创建设备 */
	mpu6050dev.device = device_create(mpu6050dev.class, NULL, mpu6050dev.devid, NULL, MPU6050_NAME); //终于可以创建设备了
	if (IS_ERR(mpu6050dev.device)) {
		return PTR_ERR(mpu6050dev.device);
	}

	mpu6050dev.private_data = client;//i2c的设备和驱动匹配成功后，client就得到了设备树相关信息，这里设备结构体变量赶紧保存起来

	return 0;
}

/*
 * @description     : i2c驱动的remove函数，移除i2c驱动的时候此函数会执行
 * @param - client 	: i2c设备
 * @return          : 0，成功;其他负值,失败
 */
static int mpu6050_remove(struct i2c_client *client)
{
	/* 删除设备 */
	cdev_del(&mpu6050dev.cdev);
	unregister_chrdev_region(mpu6050dev.devid, MPU6050_CNT);

	/* 注销掉类和设备 */
	device_destroy(mpu6050dev.class, mpu6050dev.devid);
	class_destroy(mpu6050dev.class);
	return 0;
}

/* 传统匹配方式ID列表 */
static const struct i2c_device_id mpu6050_id[] = {
	{"alientek,ap3216c", 0},  
	{}
};

/* 设备树匹配列表 */
static const struct of_device_id mpu6050_of_match[] = {
	{ .compatible = "InvenSense,mpu6050" },
	{ /* Sentinel */ }
};

/* i2c驱动结构体 */	
static struct i2c_driver mpu6050_driver = {
	.probe = mpu6050_probe,
	.remove = mpu6050_remove,
	.driver = {
			.owner = THIS_MODULE,
		   	.name = "mpu6050",
		   	.of_match_table = mpu6050_of_match, 
		   },
	.id_table = mpu6050_id,
};
		   
/*
 * @description	: 驱动入口函数
 * @param 		: 无
 * @return 		: 无
 */
static int __init mpu6050_init(void)
{
	int ret = 0;

	ret = i2c_add_driver(&mpu6050_driver);
	return ret;
}

/*
 * @description	: 驱动出口函数
 * @param 		: 无
 * @return 		: 无
 */
static void __exit mpu6050_exit(void)
{
	i2c_del_driver(&mpu6050_driver);
}

/* module_i2c_driver(mpu6050_driver) */

module_init(mpu6050_init);
module_exit(mpu6050_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("zxy");



